# EASIEAT-api

First step you have to install all dependency by **yarn install** 
yarn install

## Dependency
These modules must be installed globally
* babel-node
* nodemon
* yarn

## Basic command
* **yarn install** for install all Dependency 
* **yarn dev** run server by using nodemon.
* **yarn build** build code for production
* **yarn start** for start production server 
