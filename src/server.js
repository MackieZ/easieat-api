import express from 'express';
import compression from 'compression';
import bodyParser from 'body-parser';
import morgan from 'morgan';
import cookieParser from 'cookie-parser';
import session from 'express-session';
import routes from './routes';
import cors from 'cors';
import mongoose from 'mongoose';
import path from 'path';

require('events').EventEmitter.defaultMaxListeners = 0;

mongoose.Promise = global.Promise;
mongoose.connect('mongodb://easieat:qYJwOJUcfi@104.248.146.169:27017/easieat', {
  useNewUrlParser: true,
  useFindAndModify: false
});

const app = express();

app.use(
  cors({
    //'origin': 'http://172.20.10.6:3000',
    methods: 'GET,HEAD,PUT,PATCH,POST,DELETE',
    preflightContinue: false,
    allowedHeaders: ['X-Requested-With', 'x-access-token', 'Content-Type',],
    credentials: true,
    maxAge: 5
  })
);
const HeaderMiddleware = (req, res, next) => {
  res.header('X-Frame-Options', 'deny');
  next();
};
app.use(HeaderMiddleware);
app.use(compression());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

app.use(cookieParser());
app.use(
  session({
    resave: true,
    saveUninitialized: true,
    secret: process.env.SESSION_SECRET || 'SESSION_SECRET',
    cookie: { maxAge: 60000 }
  })
);

app.use(morgan('dev'));
app.use('/static',express.static(path.join(__dirname + '/../public')));
app.use('/', routes);

app.listen(8080, () => {
  console.log('listen on port : 8080');
});
