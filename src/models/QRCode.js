import mongoose from 'mongoose';

const deepPopulate = require('mongoose-deep-populate')(mongoose);

const { Schema } = mongoose;
const { ObjectId } = Schema.Types;
const SchemaType = mongoose.Schema.Types;

const schema = new Schema({
    restaurantID: [SchemaType.ObjectId],
    serialNumber : String,
    tableNumber: Number,
    createdAt: { type: Date, default: Date.now },
});

schema.plugin(deepPopulate);

export default mongoose.model('QRCode', schema);