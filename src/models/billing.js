import mongoose from 'mongoose';

const deepPopulate = require('mongoose-deep-populate')(mongoose);

const { Schema } = mongoose;
const { ObjectId } = Schema.Types;
const SchemaType = mongoose.Schema.Types;

const schema = new Schema({
    orderID: [SchemaType.ObjectId],
    subTotal: Number,
    tax: Number,
    discount: Number,
    total: Number,
    payWith: String,
    createdAt: { type: Date, default: Date.now },
});

schema.plugin(deepPopulate);

export default mongoose.model('billings', schema);