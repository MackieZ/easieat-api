import mongoose from 'mongoose';

const deepPopulate = require('mongoose-deep-populate')(mongoose);

const { Schema } = mongoose;
const { ObjectId } = Schema.Types;
const SchemaType = mongoose.Schema.Types;

const schema = new Schema({
    restaurantID: [SchemaType.ObjectId],
    userID: [SchemaType.ObjectId],
    queueNumber: String,
    queueType: String,
    queueName: String,
    totalPeople: Number,
    inQueue: Boolean,
    inRestaurant: Boolean,
    processQueueBy: String,
    queueActiveTime: Date,
    createdAt: { type: Date, default: Date.now },
});

schema.plugin(deepPopulate);

export default mongoose.model('Queue', schema);