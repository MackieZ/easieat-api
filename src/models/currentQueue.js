import mongoose from 'mongoose';

const deepPopulate = require('mongoose-deep-populate')(mongoose);

const { Schema } = mongoose;
const { ObjectId } = Schema.Types;

const schema = new Schema({
    restaurantID: ObjectId,
    currentQueue: Number,
    totalQueue: Number,
    queueType: String,
    updateAt: { type: Date, default: Date.now },
});

schema.plugin(deepPopulate);

export default mongoose.model('currentQueue', schema);