import mongoose from 'mongoose';

const deepPopulate = require('mongoose-deep-populate')(mongoose);

const { Schema } = mongoose;
const { ObjectId } = Schema.Types;

const schema = new Schema({
	userID: String,
	name: String,
	detail: String,
	address: String,
	tel: String,
	star: Object,
	openTime : Object,
	location: {
		longtitude : Number,
		latitude: Number
	},
	type: String,
	logo: String,
  	createdAt: { type: Date, default: Date.now },
});

schema.plugin(deepPopulate);

export default mongoose.model('restaurants', schema);