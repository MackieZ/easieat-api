import mongoose from 'mongoose';

const deepPopulate = require('mongoose-deep-populate')(mongoose);

const { Schema } = mongoose;
const { ObjectId } = Schema.Types;

const schema = new Schema({
    restaurantID: ObjectId,
    tableName: String,
    SerialNumber: String,
    status: Boolean,
    createdAt: { type: Date, default: Date.now },
});

schema.plugin(deepPopulate);

export default mongoose.model('table', schema);