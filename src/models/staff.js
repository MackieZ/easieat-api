import mongoose from 'mongoose';

const deepPopulate = require('mongoose-deep-populate')(mongoose);

const { Schema } = mongoose;
const { ObjectId } = Schema.Types;
const SchemaType = mongoose.Schema.Types;

const schema = new Schema({
    username: String,
    password: String,
    restaurantID: [SchemaType.ObjectId],
    info: {
        firstName: String,
        lastName: String,
        gender: String,
        birthDay: String
    },
    privilege: String,
    createdAt: { type: Date, default: Date.now },
});

schema.plugin(deepPopulate);

export default mongoose.model('Staff', schema);