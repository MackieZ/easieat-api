import mongoose from 'mongoose';

const deepPopulate = require('mongoose-deep-populate')(mongoose);

const { Schema } = mongoose;
const { ObjectId } = Schema.Types;

const schema = new Schema({
  email: String,
  password: String,
  memberId: String,
  name: {
    firstName: String,
    lastName: String
  },
  gender: String,
  tel: String,
  birthday: Date,
  displayImage: String,
  type: String,
  activeRestaurant: Boolean,
  restaurantID: ObjectId,
  createdAt: { type: Date, default: Date.now },
});

schema.plugin(deepPopulate);

export default mongoose.model('Users', schema);