import mongoose from 'mongoose';

const deepPopulate = require('mongoose-deep-populate')(mongoose);

const { Schema } = mongoose;
const SchemaType = mongoose.Schema.Types;

const schema = new Schema({
    userID: SchemaType.ObjectId,
    token: String,
    expiredDate: Date,
    createdAt: { type: Date, default: Date.now },
});

schema.plugin(deepPopulate);

export default mongoose.model('AuthToken', schema);