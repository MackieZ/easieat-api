import User from './user';
import Restaurant from './restaurant';
import Staff from './staff';
import RestaurantComments from './restaurantComent';
import QueueType from './queueType';
import Queue from './queue';
import QRCode from './QRCode';
import Promotion from './promotion';
import Order from './order';
import FoodType from './foodType';
import Food from './food';
import CurrentQueue from './currentQueue';
import CreditCard from './creditCard';
import Billing from './billing';
import Table from './ table';

export {
    User,
    Restaurant,
    Staff,
    RestaurantComments,
    QueueType,
    Queue,
    QRCode,
    Promotion,
    Order,
    FoodType,
    Food,
    CurrentQueue,
    CreditCard,
    Billing,
    Table
}
