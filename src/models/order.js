import mongoose from 'mongoose';

const deepPopulate = require('mongoose-deep-populate')(mongoose);

const { Schema } = mongoose;
const { ObjectId } = Schema.Types;

const schema = new Schema({
    userID: ObjectId,
    restaurantID: ObjectId,
    tableID: ObjectId,
    foodList: Object,
    foodStatus: Boolean,
    inProcess: Boolean,
    payStatus: Boolean,
    tableName: String,
    qrCode: String,
    processOrderBy: String,
    createdAt: { type: Date, default: Date.now },
});

schema.plugin(deepPopulate);

export default mongoose.model('Order', schema);