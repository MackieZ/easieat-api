import mongoose from "mongoose";

const collection = "order";
const Schema = mongoose.Schema;

const schema = new Schema(
  {
    orderId: Number,
    userId: Number,
    restaurantId: Number,
    createAt: Date,
    foodList: Array,
    tableNumber: String,
    price: Number
  },
  {
    collection
  }
);

export default mongoose.model(collection, schema);
