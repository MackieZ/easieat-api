import mongoose from 'mongoose';

const collection = 'incrementId';
const Schema = mongoose.Schema;

const schema = new Schema(
  {
    fieldName: String,
    lastId: Number
  },
  {
    collection
  }
);

export default mongoose.model(collection, schema);
