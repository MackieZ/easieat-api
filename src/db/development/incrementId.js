import IncrementId from "../incrementId";

const data = [
  {
    fieldName: "foodId",
    lastId: 10
  },
  {
    fieldName: "restaurantId",
    lastId: 3
  },
  {
    fieldName: "orderId",
    lastId: 3
  }
];

export default () => {
  IncrementId.collection.dropIndexes();
  IncrementId.remove({}, () => {
    IncrementId.create(data);
  });
};
