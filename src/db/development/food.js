import Food from '../food';

const data = [
  {
    foodId: 1,
    restaurantId: 1,
    name: 'สตอเบอร์รี่ชีสเค้ก',
    price: 30,
    url: 'http://lorempixel.com/400/400/',
    type: 'อาหารคาว'
  },
  {
    foodId: 2,
    restaurantId: 1,
    name: 'เค้กแบล็คฟอเรส',
    price: 30,
    url: 'http://lorempixel.com/400/400/',
    type: 'อาหารคาว'
  },
  {
    foodId: 3,
    restaurantId: 1,
    name: 'เครปเค้ก',
    price: 30,
    url: 'http://lorempixel.com/400/400/',
    type: 'อาหารคาว'
  },
  {
    foodId: 4,
    restaurantId: 1,
    name: 'สตอเบอร์รี่ชีสเค้ก',
    price: 30,
    url: 'http://lorempixel.com/400/400/',
    type: 'อาหารคาว'
  },
  {
    foodId: 5,
    restaurantId: 1,
    name: 'สตอเบอร์รี่ชีสเค้ก',
    price: 30,
    url: 'http://lorempixel.com/400/400/',
    type: 'อาหารคาว'
  },
  {
    foodId: 6,
    restaurantId: 2,
    name: 'สตอเบอร์รี่ชีสเค้ก',
    price: 30,
    url: 'http://lorempixel.com/400/400/',
    type: 'อาหารคาว'
  }
];

export default () => {
  Food.collection.dropIndexes();
  Food.remove({}, () => {
    Food.create(data);
  });
};
