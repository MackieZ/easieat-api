import Food from "./food";
import IncrementId from "./incrementId";
import Restaurant from "./restaurant";
import Order from "./order";

export default (req, res) => {
  if (process.env.NODE_ENV !== "production") {
    console.log("Init mongo starting ...");
    Food();
    IncrementId();
    Restaurant();
    Order();
    console.log("Init mongo finished");
    res.send("Database initialize finished");
  }
};
