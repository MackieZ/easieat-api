import Order from "../order";

const data = [
  {
    orderId: 1,
    userId: 1,
    restaurantId: 1,
    createAt: "Sat Mar 09 2019 19:48:13 GMT+0700 (Indochina Time)",
    foodList: [
      {
        foodId: 3,
        restaurantId: 1,
        name: "เครปเค้ก",
        price: 30,
        url: "http://lorempixel.com/400/400/",
        type: "ขนมเค้ก",
        status: 1
      },
      {
        foodId: 5,
        restaurantId: 1,
        name: "สตอเบอร์รี่ชีสเค้ก",
        price: 30,
        url: "http://lorempixel.com/400/400/",
        type: "ขนมเค้ก",
        status: 2
      }
    ],
    tableNumber: "001001",
    price: 60
  }
];

export default () => {
  Order.collection.dropIndexes();
  Order.remove({}, () => {
    Order.create(data);
  });
};
