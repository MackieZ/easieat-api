import mongoose from 'mongoose';

const collection = 'food';
const Schema = mongoose.Schema;

const schema = new Schema(
  {
    foodId: { type: Number, required: true },
    restaurantId: Number,
    name: String,
    type: String,
    price: Number,
    url: String
  },
  {
    collection
  }
);

export default mongoose.model(collection, schema);
