import { Restaurant,Table } from '../models';
import random from '../utils/random';
import Joi from 'joi'

export const addTable = async (req,res) => {
  const bodySchema = {
    tableName: Joi.string().required()
  };
  const { error, value } = Joi.validate(req.body, bodySchema);
  if (error) {
    return res.status(200).json({ 
      error: true,
      message: error.details[0].message,
    });
  }
  const { tableName } = value;
  const getRestaurantInfo = await Restaurant.findOne({ userID : req.userInfo.userID });
  if(getRestaurantInfo) {
    const saveTable = await new Table({
      restaurantID: getRestaurantInfo._id,
      tableName: tableName,
      SerialNumber: random(10),
      status: true,
    }).save()
    if(saveTable)
      return res.json({
        error: false,
        message: 'ทำรายการสำเร็จ',
        data: saveTable
      });
    else 
      return res.json({
        error: true,
        message: 'ทำรายการไม่สำเร็จ, กรุณาลองใหม่อีกครั้ง'
      });
  } else {
    return res.json({
      error: true,
      message: 'ผิดพลาด, กรุณาลองใหม่อีกครั้ง'
    });
  }
}

export const editTable = async (req,res) => {
  const bodySchema = {
    tableName: Joi.string().required(),
    tableID: Joi.string().required()
  };
  const { error, value } = Joi.validate(req.body, bodySchema);
  if (error) {
    return res.status(200).json({ 
      error: true,
      message: error.details[0].message,
    });
  }
  const { tableName,tableID } = value;
  const getRestaurantInfo = await Restaurant.findOne({ userID: req.userInfo.userID });
  if(getRestaurantInfo) {
    const updateTable = await Table.findOneAndUpdate({ _id: tableID, restaurantID: getRestaurantInfo._id },{
      $set: {
        tableName: tableName,
      }
    });
    if(updateTable)
      return res.json({
        error: false,
        message: 'ทำรายการสำเร็จ'
      });
    else 
      return res.json({
        error: true,
        message: 'ทำรายการไม่สำเร็จ, กรุณาลองใหม่อีกครั้ง'
      })
  } else {
    return res.json({
      error: true,
      message: 'ผิดพลาด, กรุณาลองใหม่อีกครั้ง'
    })
  }
}

export const deleteTable = async (req,res) => {
  const bodySchema = {
    tableID: Joi.string().required()
  };
  const { error, value } = Joi.validate(req.params, bodySchema);
  if (error) {
    return res.status(200).json({ 
      error: true,
      message: error.details[0].message,
    });
  }
  const { tableID } = value;
  const getRestaurantInfo = await Restaurant.findOne({ userID: req.userInfo.userID });
  if(getRestaurantInfo) {
    const updateTableStatusToDelete = await Table.findOneAndUpdate({ _id: tableID, restaurantID: getRestaurantInfo._id}, { $set: { status: false }});
    if(updateTableStatusToDelete)
      return res.json({
          error: false,
          message: 'ทำรายการสำเร็จ!'
        });
      else
        return res.json({
          error: true,
          message: 'ทำรายการไม่สำเร็จ, กรุณาลองใหม่อีกครั้ง'
        });
    } else {
      return res.json({
        error: true,
        message: 'ผิดพลาด กรุณาลองใหม่อีกครั้ง'
      });
    }
}
export const listTable = async (req,res) => {
  const getRestaurantInfo = await Restaurant.findOne({ userID : req.userInfo.userID });
  if(getRestaurantInfo) {
    const getTableList = await Table.find({ restaurantID: getRestaurantInfo._id, status: true });
    return res.json(getTableList);
  } else {
    return res.json({
      error: true,
      message: 'ผิดพลาด'
    });
  }
} 
