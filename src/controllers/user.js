import { User,Restaurant } from '../models';
import Joi from 'joi';
import jwt from 'jsonwebtoken';
import bcrypt from 'bcrypt';
import hashBcrypt from '../utils/hashBcrypt';
import Config from '../config';


export const isLogin = (req,res,next) => {
  let decoded;
  try {
    decoded = jwt.verify(req.headers['x-access-token'], Config.encryptKeys);
    req.userInfo = decoded;
    next();
  } catch(err) {
    return res.json({
      error: true,
      message: 'Login session is expired, Please login again!',
      isLogin: false
    })
  }
}

export const loginByEmail = async (req,res) => {
  const bodySchema = {
    email: Joi.string().email().lowercase().trim().required(),
    password: Joi.string().required(),
    type: Joi.string().required()
  };
  const { error, value } = Joi.validate(req.body, bodySchema);
  if (error) {
    return res.status(200).json({ 
      error: true,
      message: error.details[0].message
    });
  }
  const { email, password,type } = value;
  const userType = ['owner','user'];
  if(userType.indexOf(type) == -1) 
    return res.json({
      error: true,
      message: 'invalid'
    });
  const getUserInfo = await User.findOne({ email, type });
  if(!getUserInfo) 
    return res.status(200).json({
      error: true,
      message : 'Email or password is wrong.'
    });
  const hasPassword = bcrypt.compareSync(password, getUserInfo.password);
  if (!hasPassword) 
    return res.status(200).json({
      error: true,
      message : 'Email or password is wrong.'
    });
  const token = jwt.sign({ 
    userID: getUserInfo._id,
  }, Config.encryptKeys,{ expiresIn: '24h' });

  if(getUserInfo.type == 'owner') {
    const getRestaurantInfo = await Restaurant.find({ userID: getUserInfo._id });
    return res.status(200).send({
      error: false,
      message: 'Login success!',
      token: token,
      activeRestaurant: getUserInfo.activeRestaurant,
      restaurantID: getRestaurantInfo._id
    });
  } else {
    return res.status(200).send({
      error: false,
      message: 'Login success!',
      token: token
    });
  }
}


export const register = async (req,res) => {
  const bodySchema = {
    email: Joi.string().email().lowercase().trim().required(),
    firstName: Joi.string().required(),
    lastName: Joi.string().required(),
    tel: Joi.string().required(),
    password: Joi.string().required(),
    birthday: Joi.string().isoDate().required(),
    gender: Joi.string().required(),
    type: Joi.string().required(),
  };
  const { error, value } = Joi.validate(req.body, bodySchema);
  if (error) {
    console.log(req.body);
    return res.status(200).json({ 
      error: true,
      message: error.details[0].message,
    });
  }
  const { email, firstName, lastName, tel, password, birthday, gender, type } = value;
  if(['owner','user'].indexOf(type) == -1) 
    return res.json({
      error: true,
      message: 'invalid type'
    });
  const getUserInfo = await User.findOne({ email });
  if(getUserInfo)
    return res.status(200).json({
       message: 'Your email already exists' 
    });
    
  const passwordBcrypt = hashBcrypt(password);
  // // save info to db
  new User({
    email: email,
    password: passwordBcrypt,
    name: {
      firstName: firstName,
      lastName: lastName
    },
    gender: gender,
    tel: tel,
    birthday: birthday,
    type: type,
    activeRestaurant: false,
    displayImage: 'noavatar.jpg'
  }).save();

  return res.status(200).json({
    error: false,
    message: 'สมัครสมาชิกเสร็จสิ้น',
  });
}

export const userInfo = async (req,res) => {
  const getUserInfo = await User.findOne({ _id: req.userInfo.userID });
  console.log(getUserInfo);
  if(getUserInfo)
    return res.json({
      error: false,
      name: getUserInfo.name,
      displayImage: getUserInfo.displayImage
    });
}
