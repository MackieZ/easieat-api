import uploadFoodImage from '../storage/newFoodImage';
import { Food, Restaurant,Table } from '../models';
import Joi from 'joi';
import Config from '../config';
import R from 'ramda'
import async from 'async';

//import Food from '../db/food';
//import { getLastFoodId } from './incrementId';



// export const foodList = (req, res) => {
//   return Food.find().then(result => res.send(result));
// };

// export const deleteFood = async (req, res) => {
//   const { foodId } = req.body;

//   try {
//     const food = await Food.findOneAndDelete({ foodId });
//     if (!food) {
//       throw new Error('Food is not found');
//     }

//     return res.send(food);
//   } catch ({ message }) {
//     res.status(404).send({ error: true, message });
//   }
// };

// export const setFood = async (req, res) => {
//   const { restaurantId, name = '', price = 0, type = '' } = req.body;
//   const { file = null } = req;

//   if (!restaurantId) {
//     return res.status(400).send({ error: true, message: 'Bad Request' });
//   }

//   const path = file && file.path;

//   if (req.body.foodId) {
//     return Food.findOneAndUpdate(
//       {
//         foodId: req.body.foodId
//       },
//       {
//         foodId: req.body.foodId,
//         restaurantId: +restaurantId,
//         name,
//         price: +price,
//         url: path || req.body.url,
//         type
//       },
//       {
//         new: true
//       }
//     ).then(result => res.send(result));
//   }

//   const foodId = await getLastFoodId();
//   console.log(foodId);

//   return Food.create({
//     foodId,
//     restaurantId: +restaurantId,
//     name,
//     price: +price,
//     url: path,
//     type
//   }).then(result => res.send(result));
// };

export const deleteFood = async (req,res) => {
  const bodySchema = {
    foodID: Joi.string().required()
  };
  const { error, value } = Joi.validate(req.params, bodySchema);
    if (error) {
      return res.status(200).json({ 
        error: true,
        message: error.details[0].message,
      });
    }
    const { foodID } = value;
    const getRestaurantInfo = await Restaurant.findOne({ userID: req.userInfo.userID });
    if(getRestaurantInfo) {
      const updateFoodStatusToDelete = await Food.findOneAndUpdate({ _id: foodID, restaurantID: getRestaurantInfo._id}, { $set: { status: false }});
      if(updateFoodStatusToDelete)
        return res.json({
          error: false,
          message: 'ทำรายการสำเร็จ!',
        });
      else
        return res.json({
          error: true,
          message: 'ทำรายการไม่สำเร็จ, กรุณาลองใหม่อีกครั้ง'
        });
    } else {
      return res.json({
        error: true,
        message: 'ผิดพลาด กรุณาลองใหม่อีกครั้ง'
      });
    }
}
export const foodListApp = async (req,res) => {
  const bodySchema = {
    qrCode: Joi.string().required()
  };
  const { error, value } = Joi.validate(req.params, bodySchema);
  if (error) {
    return res.status(200).json({ 
      error: true,
      message: error.details[0].message,
    });
  }
  const { qrCode } = value;
  const getTableInfoWithQRCODE = await Table.findOne({ SerialNumber: qrCode, status: true });
  if(!getTableInfoWithQRCODE)
    return res.json({
      error: true,
      message: 'ไม่พบข้อมูล QRCODE ดังกล่าว'
    });
  const getFoodLists = await Food.find({ restaurantID: getTableInfoWithQRCODE.restaurantID, status: true });
  let response = [];
  async.each(getFoodLists, async (e, callback) => {
 //   let pick =  R.pick([`foodName`,`foodType`,`foodPrice`,`foodImage`,`createdAt`], e);
    response.push({
      name: e.foodName,
      price: e.foodPrice,
      type: e.foodType,
      foodId: e._id,
      url: Config.AppImagePath + '/foodImage/' + e.foodImage,
      createdAt: e.createdAt
    });
    callback();
  }, (err) => {
    return res.json(response);
  });
}
export const foodList = async (req,res) => {
  const getRestaurantInfo = await Restaurant.findOne({ userID: req.userInfo.userID });
  const getAllFoodList = await Food.find({ restaurantID: getRestaurantInfo._id , status: true });
  if(getRestaurantInfo && getAllFoodList) {
    let tempResp = [];
    async.each(getAllFoodList, async (e, callback) => {
      tempResp.push({
        url: Config.AppImagePath + '/foodImage/' + e.foodImage,
        name: e.foodName,
        price: e.foodPrice,
        foodId: e._id,
        type:  e.foodType
      })
      callback();
    }, (err) => {
      return res.json(tempResp);
    });
  } else {
    return res.json({
      error: true,
      message: 'ผิดพลาด, กรุณาลองใหม่อีกครั้ง!'
    })
  }
}

export const editFood = async (req,res) => {
  uploadFoodImage(req, res, async (err) => {
    const bodySchema = {
      foodId: Joi.string().required(),
      foodName: Joi.string().required(),
      foodType: Joi.string().required(),
      foodPrices: Joi.string().required(),
      file: Joi.any()
    };
    const { error, value } = Joi.validate(req.body, bodySchema);
    if (error) {
      return res.status(200).json({ 
        error: true,
        message: error.details[0].message,
      });
    }
    const { foodId, foodName, foodType,  foodPrices } = value;
    if (err){
      res.status(400).json({
        error: true,
        message: 'อัพโหลดไฟล์ไม่สำเร็จ',
        err: err
      });
    } else {
      let updateSet = {};
      try {
        const fileUploadName = req.file.filename;
        updateSet = {
          foodName: foodName,
          foodType: foodType,
          foodPrice: (foodPrices),
          foodImage: fileUploadName,
        }
      } catch (ex) {
        updateSet = {
          foodName: foodName,
          foodType: foodType,
          foodPrice: (foodPrices),
        }
      }
      const getRestaurantInfo = await Restaurant.findOne({ userID: req.userInfo.userID });
      if(getRestaurantInfo) {
        const updateFood = await Food.findOneAndUpdate({ _id: foodId, restaurantID: getRestaurantInfo._id , status: true },{
          $set: updateSet
        });
        if(updateFood)
          return res.json({
            error: false,
            message: 'ทำรายการสำเร็จ',
            data: {
              url:  Config.AppImagePath + '/foodImage/' + updateFood.foodImage, 
              name: updateFood.foodName,
              price: updateFood.foodPrice,
              foodId: foodId, 
              type: updateFood.foodType,
            }
          });
        else 
          return res.json({
            error: true,
            message: 'ทำรายการไม่สำเร็จ, กรุณาลองใหม่อีกครั้ง'
          })
      } else {
        return res.json({
          error: true,
          message: 'ผิดพลาด, กรุณาลองใหม่อีกครั้ง'
        })
      }
    }
  });
}
export const getFoodInfoByID = async (req,res) => {
  const bodySchema = {
    foodID: Joi.string().required(),
  };
 const { error, value } = Joi.validate(req.params, bodySchema);
  if (error) {
    return res.status(200).json({ 
      error: true,
      message: error.details[0].message,
    });
  }
  const { foodID } = value;
  const getRestaurantInfo = await Restaurant.findOne({ userID: req.userInfo.userID });
  if(getRestaurantInfo) {
    const getFoodInfo = await Food.findOne({ _id: foodID, restaurantID: getRestaurantInfo._id });
    if(getFoodInfo) {
      return res.json(getFoodInfo);
    } else {
      return res.json({
        error: true,
        message: 'ผิดพลาด, กรุณาลองใหม่อีกครั้ง'
      });
    }
  } else {
    return res.json({
      error: true,
      message: 'ผิดพลาด, กรุณาลองใหม่อีกครั้ง'
    });
  }
}
export const addFood = async (req,res) => {
  uploadFoodImage(req, res, async (err) => {
    const bodySchema = {
      foodName: Joi.string().required(),
      foodType: Joi.string().required(),
      foodPrices: Joi.string().required(),
      file: Joi.any()
    };
   const { error, value } = Joi.validate(req.body, bodySchema);
    if (error) {
      return res.status(200).json({ 
        error: true,
        message: error.details[0].message,
      });
    }
    const { 
      foodName, 
      foodType, 
      foodPrices, 
    } = value;

    const fileUploadName = req.file.filename;

    if (err){
      res.status(400).json({
        error: true,
        message: 'อัพโหลดไฟล์ไม่สำเร็จ',
        err: err
      });
    } else {
      const getRestaurantInfo = await Restaurant.findOne({ userID: req.userInfo.userID });
      console.log({ foodName: foodName,
        foodType: foodType,
        foodPrice: +foodPrices,
        foodImage: fileUploadName,
        restaurantID: getRestaurantInfo._id,
        status: true
      });
      const saveFood = await new Food({
        foodName: foodName,
        foodType: foodType,
        foodPrice: +foodPrices,
        foodImage: fileUploadName,
        restaurantID: getRestaurantInfo._id,
        status: true
      }).save();
      console.log(saveFood);
      if(saveFood)
        res.json({
          error: false,
          message: 'เพิ่มรายการสำเร็จ',
          data: {
            name: foodName,
            type: foodType,
            price: foodPrices,
            url: Config.AppImagePath + '/foodImage/' + fileUploadName,
            foodId: saveFood._id,
          }
        });
      else 
        res.json({
          error: true,
          message: 'เพิ่มรายการไม่สำเร็จ'
        });
    }
  });
}