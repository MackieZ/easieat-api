import R from 'ramda'
import async from 'async';
import Config from '../config';
import { Queue, CurrentQueue,Restaurant,User } from '../models';
import Joi from 'joi';

export const getQueue = async (req,res) => {
  const bodySchema = {
    restaurantID : Joi.string().required(),
  };
  const { error, value } = Joi.validate(req.body, bodySchema);
  if (error) {
    return res.status(200).json({ 
      error: true,
      message: error.details[0].message,
    });
  }
  const { restaurantID } = value;
  
  const getUserQueue = await Queue.findOne({ 
    restaurantID: restaurantID, 
    userID: req.userInfo.userID,
    inQueue: true
  });
  return res.json({
    queueNumber: getUserQueue.queueNumber,
    totalPeople: getUserQueue.totalPeople
  });
}

export const getQueueRestaurant = async (req,res) => {
  const getRestaurantInfo = await Restaurant.findOne({ userID : req.userInfo.userID });
  const getallRestaurantQueue = await Queue.find({ 
    restaurantID: getRestaurantInfo._id,
  });
  let tempResp = [];
  async.each(getallRestaurantQueue, async (e, callback) => {
    if((e.inQueue == false && e.inRestaurant == true) || (e.inQueue == true && e.inRestaurant == false)) {
      let getUserInfo = await User.findOne({ _id: e.userID })
      let pick =  R.pick([`_id`,`inQueue`,`inRestaurant`,`totalPeople`,`queueNumber`,`createdAt`,`queueName`], e);
      if(e.processQueueBy != 'owner') 
        tempResp.push({
          ...pick, 
          firstName: getUserInfo.name.firstName,
          lastName: getUserInfo.name.lastName,
        })
      else 
        tempResp.push({
          ...pick, 
        })
    }
    callback();
  }, (err) => {
    return res.json(tempResp);
  });
  
}

export const activeQueue = async (req,res) => {
  const bodySchema = {
    queueID : Joi.string().required(),
  };
  const { error, value } = Joi.validate(req.body, bodySchema);
  if (error) {
    return res.status(200).json({ 
      error: true,
      message: error.details[0].message,
    });
  }
  const { queueID } = value;

  const getRestaurantInfo = await Restaurant.findOne({ userID : req.userInfo.userID });

  let currentTime = new Date();
  const updateStatusQueue = await Queue.updateOne({ restaurantID: getRestaurantInfo._id, _id: queueID },{
    $set: {
      inQueue: false,
      inRestaurant: true,
      queueActiveTime: currentTime,
      processQueueBy: req.userInfo.userID 
    }
  });
  const updateCurrentQueue = await CurrentQueue.findOneAndUpdate({ restaurantID: getRestaurantInfo._id  },{ $inc: { currentQueue: 1 }},{ new: true, upsert: true });
  return res.status(200).json({
    error: false,
    message: 'สำเร็จ'
  });
}

export const newQueueRestaurant = async (req,res) => {
  const bodySchema = {
    Name: Joi.string().required(),
    totalPeople: Joi.number().required()
  };
  const { error, value } = Joi.validate(req.body, bodySchema);
  if (error) {
    return res.status(200).json({ 
      error: true,
      message: error.details[0].message,
    });
  }
  const { totalPeople, Name  } = value;
  const getRestaurantInfo = await Restaurant.findOne({ userID : req.userInfo.userID });
  const updateQueue = await CurrentQueue.findOneAndUpdate({ restaurantID: getRestaurantInfo._id  },{ $inc: { totalQueue: 1 }},{ new: true, upsert: true });
  const saveToQueue = new Queue({
    restaurantID: getRestaurantInfo._id,
    userID: req.userInfo.userID,
    queueName: Name,
    inQueue: true,
    inRestaurant: false,
    totalPeople: totalPeople,
    processQueueBy: 'owner',
    queueNumber: updateQueue.totalQueue,
  }).save();
  if(saveToQueue)
    return res.json({
      error: false,
      message: 'จองคิวสำเร็จ',
      yourQueue: updateQueue.totalQueue,
      totalPeople: totalPeople
    });
  else 
    return res.json({
      error: true,
      message: 'ผิดพลาด, กรุณาลองใหม่อีกครั้ง',
    });
} 

export const newQueue = async (req,res) => {
  const bodySchema = {
    restaurantID : Joi.string().required(),
    
    totalPeople: Joi.number().required()
  };
  const { error, value } = Joi.validate(req.body, bodySchema);
  if (error) {
    return res.status(200).json({ 
      error: true,
      message: error.details[0].message,
    });
  }
  const { restaurantID,totalPeople } = value;
  const getUserQueue = await Queue.findOne({ 
    restaurantID: restaurantID, 
    userID: req.userInfo.userID,
    inQueue: true,
  });
  console.log(getUserQueue);
  if(getUserQueue) {
    return res.json({
      error: true,
      message: 'คุณได้ทำการจองคิวร้านดังกล่าวไปก่อนหน้านี้เเล้ว'
    });
  } else {
    const getUserInfo = await User.findOne({ _id: req.userInfo.userID });
    const updateQueue = await CurrentQueue.findOneAndUpdate({ restaurantID: restaurantID  },{ $inc: { totalQueue: 1 }},{ new: true, upsert: true });
    const getRestaurantInfo = await Restaurant.findOne({ _id: restaurantID });
    const saveToQueue = new Queue({
      restaurantID: restaurantID,
      userID: req.userInfo.userID,
      queueName: getUserInfo.name.firstName + ' ' + getUserInfo.name.lastName,
      inQueue: true,
      inRestaurant: false,
      totalPeople: totalPeople,
      queueNumber: updateQueue.totalQueue,
    }).save();
    if(saveToQueue) {
      let queueBeforeYou = (updateQueue.totalQueue - updateQueue.currentQueue) - 1;
      if(queueBeforeYou <= 0)
        queueBeforeYou = 0;

      return res.json({
        error: false,
        message: 'จองคิวสำเร็จ',
        yourQueue: 'A'+ updateQueue.totalQueue,
        totalPeople: totalPeople,
        queueBeforeYou: queueBeforeYou,
        restaurantName: getRestaurantInfo.name,
        ownerQueue: {
          firstName: getUserInfo.name.firstName,
          lastName: getUserInfo.name.lastName
        },
        profileImage: Config.AppImagePath + '/profileImage/' + getUserInfo.displayImage,
        restaurantLogo: Config.AppImagePath + '/logo/' + getRestaurantInfo.logo
      });
    } else {
      return res.json({
        error: true,
        message: 'ผิดพลาด, กรุณาลองใหม่อีกครั้ง',
      });
    }
  }
}
