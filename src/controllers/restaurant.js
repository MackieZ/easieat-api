import { Restaurant,User, CurrentQueue, Queue } from '../models';
import Joi from 'joi';
import async from 'async';
import { getDistance } from '../utils/location';
import uploadLogoRestaurant from '../storage/newRestaurant';
import config from '../config';

export const newRestaurant = (req,res) => {

  uploadLogoRestaurant(req, res, async (err) => {
    const bodySchema = {
      restaurantName: Joi.string().required(),
      restaurantType: Joi.string().required(),
      restaurantTel: Joi.string().required(),
      restaurantDetail: Joi.string().required(),
      restaurantAddress: Joi.string().required(),
      openTime: Joi.object().required(),
      location: Joi.object().required(),
      file: Joi.any()
    };
   const { error, value } = Joi.validate(req.body, bodySchema);
    if (error) {
      return res.status(200).json({ 
        error: true,
        message: error.details[0].message,
      });
    }
    const { 
      restaurantName, 
      restaurantType, 
      restaurantTel, 
      restaurantDetail,
      restaurantAddress, 
      openTime,
      location
    } = value;
    const fileUploadName = req.file.filename;
    if (err){
      res.status(400).json({
        error: true,
        message: 'upload file fail',
        err: err
      });
    } else {
      const saveRestaurant = await new Restaurant({
        userID: req.userInfo.userID,
        name: restaurantName,
        detail: restaurantDetail,
        address: restaurantAddress,
        tel: restaurantTel,
        star: {
          average: 0,
          total: 0,
          star: 0
        },
        type: restaurantType,
        location: location,
        openTime: openTime,
        logo: fileUploadName
      }).save();
      if(saveRestaurant) {
        const updateStatusUser = await User.updateOne({ _id : req.userInfo.userID },{
          $set: {
            activeRestaurant: true,
            restaurantID: saveRestaurant._id
          }
        });
        const createCurrentQueue = await new CurrentQueue({
          restaurantID: saveRestaurant._id,
          currentQueue: 0,
          totalQueue: 0,
          queueType: 'normal',
        }).save();

        if(updateStatusUser && createCurrentQueue)
          return res.json({
            error: false,
            message: 'Restaurant has been added!'
          });
      } else {
        return res.json({
          error: true,
          message: 'something wrong!'
        });
      }
    }
  });
}
export const getUserRestaurantsList = async (req,res) => {
  // const getRestaurantList = await Restaurant.find({ userID: req.userInfo.userID  });
  // let tmpReturnResp = [];
  // getRestaurantList.forEach((item) => {
  //   tmpReturnResp.push({

  //   });
  // });

}

export const list = async (req, res) => {
  const bodySchema = {
    latitude : Joi.string().required(),
    longtitude: Joi.string().required(),
  };
  const { error, value } = Joi.validate(req.params, bodySchema);
  if (error) {
    return res.status(200).json({ 
      error: true,
      message: error.details[0].message,
    });
  }
  const { longtitude, latitude } = value;
  const getRestaurantList = await Restaurant.find({});
  if(getRestaurantList) {
    let tmp = [];
    let calculateDistance = 0;

    async.each(getRestaurantList, async (item, callback) => {

      let getQueue = await CurrentQueue.findOne({ restaurantID: item._id });
      let queueCount = (getQueue.totalQueue - getQueue.currentQueue) - 1;
      console.log(queueCount)
      if(queueCount <= 0)
        queueCount = 0;
      //console.log('OLk')
      calculateDistance = getDistance(item.location.latitude,item.location.longtitude,latitude,longtitude);
        // //if(calculateDistance <= 10) 
      tmp.push({
        id: item._id,
        name: item.name,
        description: item.detail,
        type: item.type,
        distance: calculateDistance,
        rating: item.star.average,
        queue: queueCount,
        logo: config.AppImagePath + '/logo/' + item.logo,
        openTime: item.openTime,
        address: item.address,
        tel: item.tel,
        location: item.location
      });
      callback();

    }, (err) => {
      return res.json(tmp);
    });
    // Promise.each(getRestaurantList, function(fileName) {
    //   return async () => {
    //     let getQueue = await CurrentQueue.findOne({ restaurantID: item._id });
    //     let queueCount = getQueue.currentQueue - getQueue.totalQueue;
    //     console.log('OLk')
    //     calculateDistance = getDistance(item.location.latitude,item.location.longtitude,latitude,longtitude);
    //     // //if(calculateDistance <= 10) 
    //       tmp.push({
    //         id: item._id,
    //         restaurantName: item.name,
    //         description: item.detail,
    //         type: item.type,
    //         distance: calculateDistance,
    //         rating: item.star,
    //         queue: queueCount,
    //         logo: 'https://facebook.github.io/react-native/docs/assets/favicon.png'
    //       });
    //   }
    // }).then(function() {
    //     console.log("done");
    //     return res.json(tmp);
    // });
    // getRestaurantList.forEach(async (item) => {
      
      

    //     console.log(tmp)
    // });
    // console.log("DONE");
   
    //res.json(getRestaurantList);
    // return res.json({
    //   id: getRestaurantList._id,
    //   restaurantName: getRestaurantList.name,
    //   type: getRestaurantList.type,
    //   distance: 13,
    //   rating: getRestaurantList.star,
    //   queue: 123
    // });
  }
  // res.json(
  //     [
  //         {
  //             id: 1,
  //             logo: 'https://facebook.github.io/react-native/docs/assets/favicon.png',
  //             name: "ร้านบัน's Town",
  //             type: 'ร้านอาหารญี่ปุ่น',
  //             distance: 2,
  //             rating: 4.2,
  //             queue: 0,
  //         },
  //         {
  //             id: 2,
  //             logo: 'https://facebook.github.io/react-native/docs/assets/favicon.png',
  //             name: 'ร้านชาบูโฮม',
  //             type: 'ร้านชาบู',
  //             distance: 2,
  //             rating: 4.5,
  //             queue: 0,
  //         }
  //     ]
  // );
  // DB.db
  //   .collection('collection')
  //   .find({})
  //   .toArray()
  //   .then(response => res.json(response));
};
