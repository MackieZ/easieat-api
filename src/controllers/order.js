//import Order from "../db/order";
import { Order,Table,Food,Restaurant } from '../models/';
import Joi from 'joi';
import async from 'async';
import Config from '../config';
// export const orderList = (req, res) => {
//   return Order.find().then(result => res.send(result));
// };

// export const setOrder = async (req, res) => {
//   const {
//     userId = 0,
//     restaurantId = 0,
//     createAt = "",
//     foodList = [],
//     tableNumber = '001001',
//     price = 0
//   } = req.body;

//   if (!restaurantId) {
//     return res.status(400).send({ error: true, message: "Bad Request" });
//   }

//   const orderId = await getLastOrderId();

//   return Order.create({
//     orderId,
//     userId,
//     restaurantId,
//     createAt,
//     foodList,
//     tableNumber,
//     price
//   }).then(result => res.send(result));
// };
export const orderFoodByOwner = async (req,res) => {
  const bodySchema = {
    tableID: Joi.string().required(),
    foodList: Joi.array().required()
  };
  const { error, value } = Joi.validate(req.body, bodySchema);
  if (error) {
    return res.status(200).json({ 
      error: true,
      message: error.details[0].message,
    });
  }
  const { tableID, foodList } = value;
  const getTableInfo = await Table.findOne({ _id: tableID, status : true });
  if(!getTableInfo)
    return res.json({
      error: true,
      message: 'ผิดพลาด, ไม่สามารถดำเนินการได้'
    })
    const userID = req.userInfo.userID;
    let foodLists = [];
    let totalPrice = 0;
    async.each(foodList, async (item, callback) => {
      const getFoodInfo = await Food.findOne({ _id: item.foodId });
      if(getFoodInfo) {
        totalPrice += +getFoodInfo.foodPrice;
        foodLists.push({
          name: getFoodInfo.foodName,
          price: getFoodInfo.foodPrice,
          type: getFoodInfo.foodType,
          foodId: getFoodInfo._id,
          url: Config.AppImagePath + '/foodImage/' + getFoodInfo.foodImage,
          createdAt: getFoodInfo.createdAt,
          quantity: item.quantity,
          status: 1
        });
      }
      callback();
    }, (err) => {
      const saveOrder = new Order({
        userID: userID,
        restaurantID: getTableInfo.restaurantID,
        tableID: getTableInfo._id,
        foodStatus: false,
        inProcess: false,
        payStatus: false,
        tableName: getTableInfo.tableName,
        qrCode: getTableInfo.SerialNumber,
        processOrderBy: 'owner',
        foodList: foodLists,
        totalPrice: totalPrice
      }).save();
      if(saveOrder)
        return res.json({
          error:false ,
          message: 'success'
        })
      else 
        return res.json({
          error: true,
          message: 'ผิดพลาด, กรุณาลองใหม่อีกครั้ง'
        });
    });
}

export const orderFood = async (req,res) => {
  const bodySchema = {
    qrCode: Joi.string().required(),
    foodList: Joi.array().required()
  };
  const { error, value } = Joi.validate(req.body, bodySchema);
  if (error) {
    return res.status(200).json({ 
      error: true,
      message: error.details[0].message,
    });
  }
  const { qrCode, foodList } = value;
  const userID = req.userInfo.userID;
  const getTableInfo = await Table.findOne({ SerialNumber: qrCode });
  let foodLists = [];
  let totalPrice = 0;
  async.each(foodList, async (item, callback) => {
    const getFoodInfo = await Food.findOne({ _id: item.foodId });
    if(getFoodInfo) {
      totalPrice += +getFoodInfo.foodPrice;
      foodLists.push({
        name: getFoodInfo.foodName,
        price: getFoodInfo.foodPrice,
        type: getFoodInfo.foodType,
        foodId: getFoodInfo._id,
        url: Config.AppImagePath + '/foodImage/' + getFoodInfo.foodImage,
        createdAt: getFoodInfo.createdAt,
        quantity: item.quantity,
        status: 1
      });
    }
    callback();
  }, (err) => {
    const saveOrder = new Order({
      userID: userID,
      restaurantID: getTableInfo.restaurantID,
      tableID: getTableInfo._id,
      foodStatus: false,
      inProcess: false,
      payStatus: false,
      tableName: getTableInfo.tableName,
      qrCode: getTableInfo.SerialNumber,
      processOrderBy: 'user',
      foodList: foodLists,
      totalPrice: totalPrice
    }).save();
    if(saveOrder)
      return res.json({
        error:false ,
        message: 'success'
      })
    else 
      return res.json({
        error: true,
        message: 'ผิดพลาด, กรุณาลองใหม่อีกครั้ง'
      });
  });
}

export const listOrderFood = async (req,res) => {
  const bodySchema = {
    qrCode: Joi.string().required(),
  };
  const { error, value } = Joi.validate(req.params, bodySchema);
  if (error) {
    return res.status(200).json({ 
      error: true,
      message: error.details[0].message,
    });
  }
  const { qrCode } = value;
  const userID = req.userInfo.userID;
  const getTableInfo = await Table.findOne({ SerialNumber: qrCode });
  const getOrderFood = await Order.find({ restaurantID: getTableInfo.restaurantID , foodStatus: false , userID: userID});
  let sum = 0;
  let response = [];
  async.eachSeries(getOrderFood, async (item, callback) => {
    sum = 0;
    console.log(sum)
    async.eachSeries(item.foodList, async (item_, callbackKK) => {
      let getFoodInfo = await Food.findOne({ _id: item_.foodId });
      sum = sum + ((+getFoodInfo.foodPrice) * (+item_.quantity));
      console.log(sum)
      callbackKK();
    }, (err) => {
      response.push({
        foodList: item.foodList,
        foodStatus: item.foodStatus,
        createdAt: item.createdAt,
        payStatus: item.payStatus,
        tableName: item.tableName,
        price: sum
      });
      callback();
    })
  }, (err) => {
   // console.log(response)
    return res.json(response);
  })
}
export const listOrderFromOwner = async (req,res) => {
  const bodySchema = {
    tableID: Joi.string().required(),
  };
  const { error, value } = Joi.validate(req.params, bodySchema);
  if (error) {
    return res.status(200).json({ 
      error: true,
      message: error.details[0].message,
    });
  }
  const { tableID } = value;
 // try {
    const userID = req.userInfo.userID;
    const getTableInfo = await Table.findOne({ _id: tableID });
    const getRestaurantInfo = await Restaurant.findOne({ userID: userID });
    const getOrderFood = await Order.find({ restaurantID: getRestaurantInfo._id, qrCode: getTableInfo.SerialNumber ,foodStatus: false});
    let sum = 0;
    let response = [];
    async.eachSeries(getOrderFood, async (item, callback) => {
      sum = 0;
      async.eachSeries(item.foodList, async (item_, callback_) => {
        let getFoodInfo = await Food.findOne({ _id: item_.foodId });
        console.log(getFoodInfo)
        sum += (+getFoodInfo.foodPrice) * (+item_.quantity);
        callback_();
      }, (err) => {
        response.push({
          foodList: item.foodList,
          foodStatus: item.foodStatus,
          createdAt: item.createdAt,
          payStatus: item.payStatus,
          tableName: item.tableName,
          qty: item.quantity,
          price: sum
        });
        callback();
      })
    }, (err) => {
      return res.json(response);
    })
}
export const makeFinishOrderFood = async (req,res) => {
  const bodySchema = {
    tableID: Joi.string().required(),
  };
  const { error, value } = Joi.validate(req.params, bodySchema);
  if (error) {
    return res.status(200).json({ 
      error: true,
      message: error.details[0].message,
    });
  }
  const { tableID } = value;
  const userID = req.userInfo.userID;
  const getTableInfo = await Table.findOne({ _id: tableID });
  const getRestaurantInfo = await Restaurant.findOne({ userID: userID });
  const clearOrder = await Order.updateMany({ restaurantID: getRestaurantInfo._id, qrCode: getTableInfo.SerialNumber ,foodStatus: false}, { 
    $set: {
      foodStatus: true,
      payStatus: true,
      inProcess: true
    }
  });
  return res.json({
    error: false,
    message: 'success'
  });
}

export const markFoodListFinish = async (req,res) => {
  const bodySchema = {
    tableID: Joi.string().required(),
    foodID: Joi.string().required()
  };
  const { error, value } = Joi.validate(req.body, bodySchema);
  if (error) {
    return res.status(200).json({ 
      error: true,
      message: error.details[0].message,
    });
  }
  const { tableID, foodID } = value;
  const userID = req.userInfo.userID;
  const getRestaurantInfo = await Restaurant.findOne({ userID: userID });

  const update = Order.updateOne({ 
    'tableID': tableID,
    'restaurantID': getRestaurantInfo._id,
    'foodList.foodId': foodID
  },{ 
    $set: { 
      'foodList.$.status': 3
    }
  });
  return res.json({
    error: false,
    message: 'success'
  })
}

export const markOrderToInProcess = async (req,res) => {
  const bodySchema = {
    tableID: Joi.string().required(),
  };
  const { error, value } = Joi.validate(req.params, bodySchema);
  if (error) {
    return res.status(200).json({ 
      error: true,
      message: error.details[0].message,
    });
  }
  const { tableID } = value;
  const userID = req.userInfo.userID;
  const getRestaurantInfo = await Restaurant.findOne({ userID: userID });
  const getOrderInfo = await Order.findOne({ tableID: tableID, restaurantID: getRestaurantInfo._id, foodStatus: false });
  let tmp = [];
  for(let i in getOrderInfo.foodList) {
    tmp.push({
      ...getOrderInfo.foodList[i],
      status: 2
    });
  }
  const updateStateToInProcess = await Order.updateMany({ tableID: tableID, restaurantID: getRestaurantInfo._id, foodStatus: false }, { 
    $set: {
      inProcess: true,
      foodList: tmp
    }
  });
  return res.json({
    error: false,
    message: 'success',
  });
}

export const getOrderByOwner = async (req,res) => {
  const userID = req.userInfo.userID;
  const getRestaurantInfo = await Restaurant.findOne({ userID: userID });
  const getOrderFood = await Order.find({ restaurantID: getRestaurantInfo._id , foodStatus: false });
  let response = [];
  async.eachSeries(getOrderFood, async (item, callback) => {
    response.push({
      tableID: item.tableID,
      tableName: item.tableName,
      orderList: item.foodList,
      status: item.inProcess
    });
    callback();
  }, (err) => {
    return res.json(response);
  })
}