import { Router } from "express";
import { addTable, editTable, deleteTable, listTable } from '../controllers/tables';
const router = Router();

router.get("/list", listTable);
router.post("/add", addTable);
router.post("/edit", editTable);
router.get("/remove/:tableID", deleteTable);

export default router;
