import { Router } from 'express';

import { 
    getQueue, 
    newQueue,
    activeQueue,
    getQueueRestaurant,
    newQueueRestaurant 
} from '../controllers/queue';


const router = Router();


//router.get('/:restaurantID',getQueue);
router.post('/newqueue',newQueue);
router.post('/activequeue',activeQueue);
router.get('/getqueue',getQueue);
router.get('/getrestaurantqueue',getQueueRestaurant);
router.post('/newrestaurantqueue',newQueueRestaurant);
// router.post('/login',login);
// router.post('/register',register);


//router.get('/getallinfo',dashboard);


export default router;

