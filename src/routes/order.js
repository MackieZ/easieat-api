import { Router } from "express";
import { listOrderFood, orderFood,orderFoodByOwner,listOrderFromOwner,makeFinishOrderFood,markFoodListFinish, getOrderByOwner , markOrderToInProcess} from "../controllers/order";

const router = Router();

router.get("/list/:qrCode", listOrderFood);
router.get('/listByowner/:tableID',listOrderFromOwner)
router.get('/bill/:table');
router.post('/add', orderFood);
router.post('/addbyowner',orderFoodByOwner);
router.get('/pay/:tableID',makeFinishOrderFood);

router.post('/markfinish',markFoodListFinish);
router.get('/markinprocess/:tableID',markOrderToInProcess);
router.get('/getorder',getOrderByOwner);

export default router;
