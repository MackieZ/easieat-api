import { Router } from "express";
import users from "./users";
import restaurant from "./restaurant";
import queue from "./queue";
import food from "./food";
import order from "./order";
import table from './table';
import { isLogin } from '../controllers/user';
import initMongo from "../db/development/initMongo";

// Route group start here!
const prefix = "/api/v1";

const router = Router();

// //router.use(prefix + "/users",isLogin , users); // example check authen all route in users after login userInfo will contain in req.userInfo 

router.use(prefix + "/users", users);

router.use(prefix + "/restaurant", isLogin, restaurant);
router.use(prefix + "/queue",isLogin, queue);
router.use(prefix + "/food", isLogin, food);
router.use(prefix + "/order",isLogin, order);
router.use(prefix + "/table", isLogin, table);

router.use("/initMongo", initMongo);

export default router;
