import { Router } from 'express';
import { addFood, foodList, editFood, deleteFood, getFoodInfoByID, foodListApp } from '../controllers/food';

const router = Router();

//router.get('/list', foodList);
// router.post('/setFood', upload.single('foodImage'), setFood);
// router.post('/deleteFood', deleteFood);
router.post('/edit',editFood);
router.post('/addfood', addFood);
router.get('/list', foodList);
router.get('/info/:foodID',getFoodInfoByID);
router.get('/list/mobile/:qrCode',foodListApp);
router.get('/remove/:foodID',deleteFood);


export default router;
