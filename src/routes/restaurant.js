import { Router } from 'express';

import { list, newRestaurant } from '../controllers/restaurant';


const router = Router();


router.get('/list/:latitude/:longtitude', list);
router.post('/new',newRestaurant);

// router.post('/login',login);
// router.post('/register',register);


//router.get('/getallinfo',dashboard);


export default router;

