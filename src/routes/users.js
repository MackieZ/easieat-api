import { Router } from 'express';
import { isLogin } from '../controllers/user';
import { loginByEmail, register, userInfo } from '../controllers/user';

const router = Router();

router.get('/', (req, res) => {
    res.json({
        error: true,
        message: 'Hello!'
    });
});
router.post('/login',loginByEmail);
router.post('/register',register);

router.get('/userinfo', isLogin, userInfo);



export default router;