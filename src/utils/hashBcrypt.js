import bcrypt from 'bcrypt';

const hashBcrypt = (text) => {
  const saltRounds = 10;
  const salt = bcrypt.genSaltSync(saltRounds);
  return bcrypt.hashSync(text, salt);
};

export default hashBcrypt;