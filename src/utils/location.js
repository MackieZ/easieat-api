export const degreeToRadius = (degree) => {
    return degree * (Math.PI / 180);
}
export const getDistance = (latitude_1,longtitude_1,latitude_2,longtitude_2) => {
    var R = 6371; 
    var dLatitude = degreeToRadius(latitude_2 - latitude_1);  
    var dLongtitude = degreeToRadius(longtitude_2 - longtitude_1); 
    var temp_1 = Math.sin(dLatitude / 2) * Math.sin(dLatitude / 2) + 
                Math.cos(degreeToRadius(latitude_1)) * Math.cos(degreeToRadius(latitude_2)) * 
                Math.sin(dLongtitude / 2) * Math.sin(dLongtitude / 2); 
    var c = 2 * Math.atan2(Math.sqrt(temp_1), Math.sqrt(1 - temp_1)); 
    return R * c;
}


