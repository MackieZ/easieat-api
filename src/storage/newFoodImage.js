import multer from 'multer';
import random from '../utils/random';
import config from '../config';
import path from 'path';

const imageFilter = function (req, file, cb) {
  if (!file.originalname.match(/\.(jpg|jpeg|png)$/)) {
    return cb(new Error('Only image files are allowed!'), false);
  }
  cb(null, true);
};

let storage = multer.diskStorage({
  destination: (req, file, cb) => {
 
    cb(null, './'+ config.BaseImagePath + '/foodImage/');
  },
  filename: (req, file, cb) => {
    cb(null, random(16) + '_' + Date.now() + path.extname(file.originalname));
  }
});

export default multer({ storage: storage, fileFilter: imageFilter }).single('file');